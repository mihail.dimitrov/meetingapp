var loadMenu = function(path) {
    $.get(path + 'data/menu.json', function(response) {
        console.log(response);
        //$('#menu ul li:not(:first)').remove();
        $('#menu ul li').remove();
        $.each(response.MenuItems, function(index, val) {
            if (val.component === 'home') {
                $('#menu ul').append('<li><a data-rel="close" href="/">' + val.title + '</a></li>');
            } else {
                $('#menu ul').append('<li><a data-rel="close" href="' + path + 'modules/' + val.component + '/template.html">' + val.title + '</a></li>');
            }
        });
    });
}